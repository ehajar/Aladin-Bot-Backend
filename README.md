"#Aladin-Bot" 

```
| src
|___ main
|    |___kotlin
|    |    |___net
|    |        |___thexcoders
|    |            |___aladin_bot_backend
|    |                |___controllers
|    |                |    |   [Controller 1]
|    |                |    |   [Controller 2]
|    |                |    |   [Controller 3]
|    |                |    |   ...
|    |                |
|    |                |___converters
|    |                |    |   [Converter 1]
|    |                |    |   [Converter 2]
|    |                |    |   [Converter 3]
|    |                |    |   ...
|    |                |
|    |                |___exceptions
|    |                |    |   [Exception 1]
|    |                |    |   [Exception 2]
|    |                |    |   [Exception 3]
|    |                |    |   ...
|    |                |
|    |                |___models
|    |                |    |   [Model 1]
|    |                |    |   [Model 2]
|    |                |    |   [Model 3]
|    |                |    |   ...
|    |                |
|    |                |___nlp_models
|    |                |    |   [NlpModel 1]
|    |                |    |   [NlpModel 2]
|    |                |    |   [NlpModel 3]
|    |                |    |   ...
|    |                |
|    |                |___repositories
|    |                |    |   [Repo 1]
|    |                |    |   [Repo 2]
|    |                |    |   [Repo 3]
|    |                |    |   ...
|    |                |   
|    |                |     AladinBotBackEndApplication.kt (MAIN)
|    |___ressources
|        |   [NLP MODEL 1]
|        |   [NLP MODEL 2]
|        |   [NLP MODEL 3]
|        |   ...
|___ test
|    |___kotlin
|            |___net
|                |___thexcoders
|                    |___aladin_bot_backend
|                        |___converters
|                        |    |   [Converter 1]
|                       |    |   [Converter 2]
|                        |    |   [Converter 3]
|                        |    |   ...
|                        |
|                        |___nlp_models
|                        |    |   [NlpModel 1]
|                        |    |   [NlpModel 2]
|                        |    |   [NlpModel 3]
|                        |    |   ...
```